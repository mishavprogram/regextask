import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.training.Model;
import ua.training.View;
import ua.training.controllers.NoteConsoleInputController;

/**
 * Test class that check controller for entering Note
 *
 * @author Misha Vinnichuk
 * @version 1.0
 * @since 2017-11-10
 */

public class TestNoteConsoleInputController {
    private static NoteConsoleInputController noteConsoleInputController;

    @BeforeClass
    public static void init() {
        noteConsoleInputController = new NoteConsoleInputController(new Model(), new View());
    }

    @Test
    public void simpleTestName() {
        Assert.assertEquals(noteConsoleInputController.isName("Misha"), true);
    }

    @Test
    public void testInitName() {
        Assert.assertEquals(noteConsoleInputController.isName("misha"), false);
        Assert.assertEquals(noteConsoleInputController.isName("1misha"), false);
    }

    @Test
    public void testInitEmail() {
        Assert.assertEquals(noteConsoleInputController.isEmail("misha@ukr"), false);
        Assert.assertEquals(noteConsoleInputController.isEmail("misha@ukr.net"), true);
        Assert.assertEquals(noteConsoleInputController.isEmail("yandex"), false);
    }
}
