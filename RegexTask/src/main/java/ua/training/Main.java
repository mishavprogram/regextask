package ua.training;

import ua.training.controllers.Controller;

public class Main {
    public static void main(String[] args) {
        new Controller(new Model(), new View()).processUser();
    }
}
