package ua.training;

public class DublicateLoginInNoteException extends Exception {
    private String loginData;

    public String getLoginData() {
        return loginData;
    }

    public DublicateLoginInNoteException(String message, String loginData) {
        super(message);
        this.loginData = loginData;
    }
}
