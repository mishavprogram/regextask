package ua.training.entities;

/**
 * Note class
 * @author  Misha Vinnichuk
 * @version 1.0
 * @since   2017-11-10
 */

public class Note {
    private String name;
    private String login;
    private String email;

    public Note(String name, String surname, String email) {
        this.name = name;
        this.login = surname;
        this.email = email;
    }

    public String getLogin()
    {
        return login;
    }
}
