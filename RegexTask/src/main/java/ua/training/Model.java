package ua.training;

import ua.training.entities.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * Model class
 *
 * @author Misha Vinnichuk
 * @version 1.0
 * @since 2017-11-10
 */
public class Model {
    private List<Note> notes;
    private final static Note adminInfo = new Note("Misha", "mishav", "mishavprogram@ukr.net");

    public Model() {
        notes = new ArrayList<>();
    }

    public void insertNote(Note note)throws DublicateLoginInNoteException {
            if (note.getLogin().equals(adminInfo.getLogin())) throw new DublicateLoginInNoteException("Not unique login", note.getLogin());
            notes.add(note);
    }
}
