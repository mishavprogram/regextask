package ua.training.controllers;

import ua.training.DublicateLoginInNoteException;
import ua.training.Model;
import ua.training.entities.Note;
import ua.training.View;

import java.util.Scanner;

import static ua.training.View.*;

/**
 * Class that control entering of Note
 *
 * @author Misha Vinnichuk
 * @version 1.0
 * @since 2017-11-10
 */

public class NoteConsoleInputController {
    private Model model;
    private View view;

    private Note note;

    private String name;
    private String login;
    private String email;

    public NoteConsoleInputController(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void createNote() {
        Scanner scanner = new Scanner(System.in);

        initAllFields(scanner);

        this.note = new Note(name, login, email);
    }

    private void initAllFields(Scanner scanner) {
        print(NAME_PLEASE);
        initName(scanner);

        print(LOGIN_PLEASE);
        initLogin(scanner);

        print(EMAIL_PLEASE);
        initEmail(scanner);
    }

    public Note getNote() {
        return note;
    }

    public boolean isName(String name) {
        if (name.matches(getRegex(NAME_REGEX))) {
            return true;
        } else return false;
    }

    public boolean isEmail(String email) {
        if (email.matches(getRegex(EMAIL_REGEX))) {
            return true;
        } else return false;
    }

    public boolean isLogin(String surname) {
        if (surname.matches(getRegex(LOGIN_REGEX))) {
            return true;
        } else return false;
    }

    private void initName(Scanner scanner) {
        String userText;
        while (scanner.hasNext()) {
            userText = scanner.next();
            if (isName(userText)) {
                this.name = userText;
                break;
            } else {
                print(NAME_ERROR);
            }
        }
    }

    private void initEmail(Scanner scanner) {
        String userText;
        while (scanner.hasNext()) {
            userText = scanner.next();
            if (isEmail(userText)) {
                this.email = userText;
                break;
            } else {
                print(EMAIL_ERROR);
            }
        }
    }

    private void initLogin(Scanner scanner) {
        String userText;
        while (scanner.hasNext()) {
            userText = scanner.next();
            if (isLogin(userText)) {
                this.login = userText;
                break;
            } else {
                print(LOGIN_ERROR);
            }
        }
    }

}
