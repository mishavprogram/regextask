package ua.training.controllers;

import ua.training.DublicateLoginInNoteException;
import ua.training.Model;
import ua.training.entities.Note;
import ua.training.View;

/**
 * Main Controller class
 *
 * @author Misha Vinnichuk
 * @version 1.0
 * @since 2017-11-10
 */
public class Controller {
    Model model;
    View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void processUser() {
        NoteConsoleInputController noteConsoleInputController = new NoteConsoleInputController(model, view);

        noteConsoleInputController.createNote();

        while (true) {
            try {
                model.insertNote(noteConsoleInputController.getNote());
                break;
            } catch (DublicateLoginInNoteException e) {
                e.printStackTrace();
                view.print("Not unique login : "+e.getLoginData());
                noteConsoleInputController.createNote();
            }
        }

    }

}
