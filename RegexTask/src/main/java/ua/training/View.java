package ua.training;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * View class
 *
 * @author Misha Vinnichuk
 * @version 1.0
 * @since 2017-11-10
 */

public class View {
    public static final String EMAIL_REGEX = "regex.email";
    public static final String NAME_REGEX = "regex.name";
    public static final String LOGIN_REGEX = "regex.login";

    public static final String NAME_PLEASE = "input.please.name";
    public static final String EMAIL_PLEASE = "input.please.email";
    public static final String LOGIN_PLEASE = "input.please.login";

    public static final String NAME_ERROR = "input.error.name";
    public static final String LOGIN_ERROR = "input.error.login";
    public static final String EMAIL_ERROR = "input.error.email";

    // Resource Bundle Installation's
    public static final String MESSAGES_BUNDLE_NAME = "strings";
    public static final ResourceBundle bundle =
            ResourceBundle.getBundle(
                    MESSAGES_BUNDLE_NAME,
                    //new Locale("ua"));  // Ukrainian
                    //new Locale("ru");// Russian
                    new Locale("en"));        // English

    public static void print(String message) {
        try {
            System.out.println(bundle.getString(message));
        }
        catch (MissingResourceException ex)
        {
            System.out.println(message);
        }
    }

    public static String getRegex(String regex) {
        return bundle.getString(regex);
    }
}
